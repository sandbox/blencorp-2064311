Floating Action Buttons
=======================

A simple module to float the Drupal actions buttons (Save, Preview, etc..)
making them pinned at the bottom of the browser instead of the bottom of
the administration page.

Background
==========
This module is written to simplify the amount it takes scrolling down long admin
pages to just click the action buttons.

A similar functionality is discussed and a patch is created to be added to D8
core. (https://drupal.org/node/752482)

Installation
============

- In order to install Floating Buttons, simply place it in your module directory
(normally located at sites/all/modules).
